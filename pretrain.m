function [ pca_coeff, gmm, all_train_files, all_train_labels, all_test_files, all_test_labels ] = pretrain(params)

%PRETRAIN  Subsample DTF features, calculate PCA coefficients and train GMM model
%   Inputs:
%       params - structure of parameters
%       t_type - 'train' or 'test'
%
%   Outputs:
%       pca_coeff - PCA coefficients for each DTF feature
%       gmm - GMM params for each DTF feature

% To construct Fisher vector, fist to estimate the parameters of GMM.
% To estimate GMM params, subsampling vectors from DTF descriptors.
gmm_params.cluster_count=params.K;
gmm_params.maxcomps=gmm_params.cluster_count/4;
gmm_params.GMM_init= 'kmeans';
gmm_params.pnorm = single(2);    % L2 normalization, 0 to disable
gmm_params.subbin_norm_type = 'l2';
gmm_params.norm_type = 'l2';
gmm_params.post_norm_type = 'none';
gmm_params.pool_type = 'sum';
gmm_params.quad_divs = 2;
gmm_params.horiz_divs = 3;
gmm_params.kermap = 'hellinger';


dtf_feat_num=length(params.feat_list); %HOG HOF .....
pca_coeff=cell(dtf_feat_num,1); % PCA coeficients
gmm=cell(dtf_feat_num,1);   % GMM parameters

feat_sample_train=params.train_sample_data;  %location of training file of subsampled data
feat_sample_test=params.test_sample_data;	%location of testing file


fprintf('Subsampling DTF features ...\n');
if ~exist(feat_sample_train,'file')
    [feats_train, all_train_files, all_train_labels]=subsample(params,'train');
    save(feat_sample_train, 'feats_train','all_train_labels','all_train_files','-v7.3');
else
    load(feat_sample_train);
end


if ~exist(feat_sample_test,'file')
    [~, all_test_files, all_test_labels]=subsample(params,'test');
    save(feat_sample_test,'all_test_labels','all_test_files','-v7.3');
else
    load(feat_sample_test);
end

% TRY to parrellize it 
parfor i=1:dtf_feat_num,
	feat=feats_train{i}';
	feat_mean=mean(feat);
	feat=feat-repmat(feat_mean,size(feat,1),1); %subtract mean
	% L1 normalization & Square root
	%feat=sqrt(feat/norm(feat,1)); @ANkit -THIS DOESNT WORK AS TRAJECTORY FEATURES ARE -VE
	% Do PCA on train/test data to half-size original descriptors
	fprintf('Doing PCA ...\n');
	coeff = princomp(feat);
	coeff = coeff(:, 1:floor(size(feat,2)/2))';
	% dimensionality reduction
	pca_coeff{i}.coeff =coeff;
	pca_coeff{i}.feat_mean=feat_mean;
	feat = feat*coeff';
	fprintf('Training Guassian Mixture Model ...\n');
	gmm{i}=gmm_gen_codebook(feat',gmm_params);
end

end



