function [X,file_list,labels] = subsample(params,t_type)
% SUBSAMPLE subsample DTF features
% outputs:
%	X - cell of DTF features
%	labels - labels of corresponding DTF features
%	file_list - list of all files

X=cell(length(params.feat_list),1);
labels=[];

switch t_type
    case 'train'
        tt_list_dir=params.train_list_dir;
	CNN_list=params.CNN_dir;
	CNN_reg='*.mat';
        reg_pattern='train*';
    case 'test'
        tt_list_dir=params.test_list_dir;
	CNN_list=params.CNN_dir_test;
	CNN_reg='*.mat';
        reg_pattern='test*';
    otherwise
        error('Unknown file pattern!');
end

% extract training/test list
tt_list=[]; % train/test files
tlists=dir(fullfile(tt_list_dir,reg_pattern));
CNNlist=dir(fullfile(CNN_list,CNN_reg));
for i=1:length(tlists)
    fid=fopen(fullfile(tt_list_dir,tlists(i).name));
    tmp=textscan(fid,'%s%d','delimiter',',');
    tt_list=[tt_list;tmp{1}];
    labels=[labels;tmp{2}];
end

% Make sure all files are compressed
%check_gzip_cmd=sprintf('sh ./gzip_dtf_files -i %s > /dev/null 2>&1',params.dtf_dir);
%system(check_gzip_cmd);


switch t_type
    case 'train'
        % extract DTF data and set labels
	%Following loop will extract features from all video files
	%and store feature in their respective matrix
        TRA=[];	HOG=[]; HOF=[]; MBHx=[]; MBHy=[];CNN=[];
	parfor_progress(length(tt_list)); % Initialize 

  	parfor i=1:length(tt_list),
		parfor_progress; % Count 
	    	action=regexprep(tt_list{i},'/v_(\w*)\.avi','');
            	%act_dir=fullfile(params.dtf_dir,action);
		act_dir=params.dtf_dir;
            	clip_name=regexprep(tt_list{i},'\.avi$',''); % get video clip name
            	clip_name=regexprep(clip_name,'.*/','');
            	dtf_file=fullfile(act_dir,[clip_name,'.txt']); %change it based on dtf feature extension
            	[newTRA,newHOG,newHOF,newMBHx,newMBHy]=extract_dtf_feats(dtf_file,params,params.DTF_subsample_num);
		[newCNN]=extract_CNN_fea(fullfile(CNN_list,CNNlist(i).name),params,params.DTF_subsample_num);
		
		TRA=[TRA newTRA];
		HOG=[HOG newHOG];
		HOF=[HOF newHOF];
		MBHx=[MBHx newMBHx];
		MBHy=[MBHy newMBHy];
		CNN=[CNN newCNN];		
        end
        %matlabpool close
	X{1}=TRA;
	clear TRA;
        X{2}=HOG;
        clear HOG;
        X{3}=HOF;
        clear HOF;
        X{4}=MBHx;
        clear MBHx;
        X{5}=MBHy;
        clear MBHy;
	X{6}=CNN;
	clear CNN;
    case 'test'
        % Do nothing for test videos
    otherwise
        error('Unknown file pattern!');
end

file_list=tt_list;

end

