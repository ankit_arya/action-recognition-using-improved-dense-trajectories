Implementation of Action Recognition using Improved Dense Trajectories combined with CNN
=======================================================================

1) Instructions to run:
-----------------------
To generate fisher vectors , you need to provide location of extracted Dense Trajectory features on Hollywood 2 dataset:

run master.m

2) Dependencies:
----------------
    i) VLFEAT
    ii) sci-kit, ipython
    iii) vgg_fisher

3) Datasets:
------------
   i) Hollywood2 Dataset

4) Paper:
---------
     Action Recognition with Improved Trajectories
     http://hal.inria.fr/hal-00873267/en


Project Report
---------------------
https://bytebucket.org/ankit_arya/action-recognition-using-improved-dense-trajectories/raw/8275facc54f9bd1efec44ce83fef2a509abb65c5/ProjectReport2.pdf